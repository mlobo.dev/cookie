import React, { Component } from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import biscoitoFechado from "./src/img/biscoito.png";
import biscoitoAberto from "./src/img/biscoitoAberto.png";
import Biscoito from "./src/components/Biscoito";
class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            textoFrase: "hello",
            img: biscoitoFechado,
        };

        this.quebrarBiscoito = this.quebrarBiscoito.bind(this);

        this.frases = [
            "Hello Wolrd",
            "Abaxi com canela da dor de pé",
            "Digi escolhido",
            "de novo não.......! :(",
            "Você de novo?!",
            "Para de me apertar",
            "Pendei",
        ];
    }

    async quebrarBiscoito() {
        let numAleatorio = Math.floor(Math.random() * this.frases.length);
        response = await fetch(
            "https://allugofrases.herokuapp.com/frases/random"
        )
            .then((response) => {
                return response.json();
            })
            .catch(() => {
                this.setState({
                    textoFrase: this.frases[numAleatorio],
                    img: biscoitoAberto,
                });
            });

        console.log(response);

        this.setState({
            textoFrase: response.frase,
            img: biscoitoAberto,
        });

        setTimeout(() => {
            this.setState({
                textoFrase: "",
                img: biscoitoFechado,
            });
        }, 5000);
    }

    render() {
        return (
            <View style={styles.container}>
                <Biscoito urlImg={this.state.img} />
                <Text style={styles.textoFrase}>{this.state.textoFrase}</Text>
                <TouchableOpacity
                    style={styles.btn}
                    onPress={this.quebrarBiscoito}
                >
                    <View style={styles.btnArea}>
                        <Text style={styles.btnTexto}>Quebrar biscoito</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
    },

    btnArea: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },

    btn: {
        width: 230,
        height: 50,
        borderWidth: 2,
        borderColor: "#dd7b22",
        borderRadius: 25,
    },

    btnTexto: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#dd7b22",
    },

    textoFrase: {
        fontSize: 20,
        color: "#dd7b22",
        margin: 30,
        fontStyle: "italic",
    },
});

export default App;
