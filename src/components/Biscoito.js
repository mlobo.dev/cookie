import React from "react";
import { Image } from "react-native";

export default function Biscoito({ urlImg }) {
    return <Image source={urlImg} style={{ width: 250, height: 250 }} />;
}
